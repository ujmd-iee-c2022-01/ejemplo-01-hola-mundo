using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ejemplo_1_IEE
{
    public partial class Form1 : Form
    {
        const double Pi = 3.14159265359;
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            label1.Text = "Hola Mundo " + Pi.ToString("0.000");
            this.Text = "Hola Mundo de IEE";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            label1.Text = "Adiós Mundo " + Pi.ToString("0.0000");
            this.Text = "Adiós Mundo de IEE";
        }
    }
}
